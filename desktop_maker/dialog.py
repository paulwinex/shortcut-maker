from PySide2.QtWidgets import *
from desktop_maker.widgets import dialog_UI
from desktop_maker import maker
import traceback, os

icons_root = '/usr/share/icons'


class ShortcutMakerDialog(QWidget, dialog_UI.Ui_Form):
    def __init__(self):
        super(ShortcutMakerDialog, self).__init__()
        self.setupUi(self)
        self.create_btn.clicked.connect(self.on_create)
        self.save_path_le.setText('~/.local/share/applications')
        self.browse_icon_btn.clicked.connect(self.browse_icon)
        self.icon_le.textEdited.connect(self.fix_path)

    def on_create(self):
        data = dict(
            executable=self.exec_le.text(),
            name=self.name_le.text(),
            icon=self.icon_le.text(),
            comment=self.comment_le.text(),
            terminal=self.terminal_cbx.isChecked(),
            path=self.path_le.text(),
            save_path=self.save_path_le.text(),
            force=self.forse_cbx.isChecked()
        )
        try:
            path = maker.make(**data)
            os.popen('xdg-open '+os.path.dirname(path))
        except Exception as e:
            traceback.print_exc()
            QMessageBox.critical(self, 'Error', str(e))
            return

    def browse_icon(self):
        os.popen('xdg-open '+icons_root)
        # f = QFileDialog.getOpenFileName(self, 'Select Icon', icons_root)
        # print(f)

    def fix_path(self):
        f = 'file://'
        path = self.icon_le.text()
        if path.startswith(f):
            path = path[len(f):]
            self.icon_le.blockSignals(True)
            self.icon_le.setText(path)
            self.icon_le.blockSignals(False)


