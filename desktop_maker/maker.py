import os, subprocess

template = '''#!/usr/bin/env xdg-open
[Desktop Entry]
Name={name}
Comment={comment}
GenericName={gen_name}
Keywords={keywords}
Exec={executable}
Terminal={terminal}
Type=Application
Icon={icon}
Categories=
Path={path}
'''


def make(executable, name, icon=None, comment=None, terminal=True, path=None, keywords=None, force=False,
         save_path=None):
    if keywords is not None:
        assert isinstance(keywords, list)
    else:
        keywords = []
    executable = _get_executable(executable)
    text = template.format(
        name=name,
        gen_name=name,
        comment=comment or '',
        keywords=';'.join(keywords),
        executable=executable,
        terminal=str(terminal).lower(),
        icon=icon or '',
        path=path or ''
    )
    filename = name.lower().replace(' ', '_')
    save_path = os.path.expanduser(save_path or '~/.local/share/applications')
    save_file = os.path.join(save_path, '{}.desktop'.format(filename))
    if os.path.exists(save_file) and not force:
        raise IOError('File already exists')
    with open(save_file, 'w') as f:
        f.write(text)
    os.chmod(save_file, 0o755)
    return save_file


def _get_executable(path):
    if os.path.exists(os.path.expanduser(path.split(' ')[0])):
        return path
    try:
        new_path = subprocess.check_output(['which', path.split(' ')[0]]).decode().strip()
    except:
        raise IOError('Path of executable not found')
    return path
