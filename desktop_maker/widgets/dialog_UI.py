# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'dialog.ui'
##
## Created by: Qt User Interface Compiler version 5.14.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(818, 268)
        self.gridLayout = QGridLayout(Form)
        self.gridLayout.setObjectName(u"gridLayout")
        self.create_btn = QPushButton(Form)
        self.create_btn.setObjectName(u"create_btn")

        self.gridLayout.addWidget(self.create_btn, 7, 0, 1, 3)

        self.label_5 = QLabel(Form)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_5, 3, 0, 1, 1)

        self.label_4 = QLabel(Form)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_4, 5, 0, 1, 1)

        self.terminal_cbx = QCheckBox(Form)
        self.terminal_cbx.setObjectName(u"terminal_cbx")

        self.gridLayout.addWidget(self.terminal_cbx, 5, 1, 1, 1)

        self.label_2 = QLabel(Form)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)

        self.name_le = QLineEdit(Form)
        self.name_le.setObjectName(u"name_le")

        self.gridLayout.addWidget(self.name_le, 0, 1, 1, 2)

        self.label = QLabel(Form)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)

        self.exec_le = QLineEdit(Form)
        self.exec_le.setObjectName(u"exec_le")

        self.gridLayout.addWidget(self.exec_le, 1, 1, 1, 2)

        self.path_le = QLineEdit(Form)
        self.path_le.setObjectName(u"path_le")

        self.gridLayout.addWidget(self.path_le, 3, 1, 1, 2)

        self.label_6 = QLabel(Form)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_6, 6, 0, 1, 1)

        self.label_3 = QLabel(Form)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)

        self.icon_le = QLineEdit(Form)
        self.icon_le.setObjectName(u"icon_le")

        self.gridLayout.addWidget(self.icon_le, 2, 1, 1, 1)

        self.browse_icon_btn = QPushButton(Form)
        self.browse_icon_btn.setObjectName(u"browse_icon_btn")
        self.browse_icon_btn.setMaximumSize(QSize(30, 16777215))

        self.gridLayout.addWidget(self.browse_icon_btn, 2, 2, 1, 1)

        self.label_7 = QLabel(Form)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_7, 4, 0, 1, 1)

        self.comment_le = QLineEdit(Form)
        self.comment_le.setObjectName(u"comment_le")

        self.gridLayout.addWidget(self.comment_le, 4, 1, 1, 1)

        self.save_path_le = QLineEdit(Form)
        self.save_path_le.setObjectName(u"save_path_le")

        self.gridLayout.addWidget(self.save_path_le, 6, 1, 1, 1)

        self.forse_cbx = QCheckBox(Form)
        self.forse_cbx.setObjectName(u"forse_cbx")
        self.forse_cbx.setChecked(True)

        self.gridLayout.addWidget(self.forse_cbx, 6, 2, 1, 1)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Shortcut Maker", None))
        self.create_btn.setText(QCoreApplication.translate("Form", u"Create", None))
        self.label_5.setText(QCoreApplication.translate("Form", u"Path", None))
        self.label_4.setText(QCoreApplication.translate("Form", u"Terminal", None))
        self.terminal_cbx.setText("")
        self.label_2.setText(QCoreApplication.translate("Form", u"Name", None))
        self.name_le.setPlaceholderText(QCoreApplication.translate("Form", u"Icon Name", None))
        self.label.setText(QCoreApplication.translate("Form", u"Exec", None))
        self.exec_le.setPlaceholderText(QCoreApplication.translate("Form", u"Executable Command", None))
        self.path_le.setPlaceholderText(QCoreApplication.translate("Form", u"Work Dir", None))
        self.label_6.setText(QCoreApplication.translate("Form", u"Save To", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"Icon", None))
        self.icon_le.setPlaceholderText(QCoreApplication.translate("Form", u"Icon Image", None))
        self.browse_icon_btn.setText(QCoreApplication.translate("Form", u"...", None))
        self.label_7.setText(QCoreApplication.translate("Form", u"Comment", None))
        self.comment_le.setPlaceholderText("")
        self.save_path_le.setPlaceholderText(QCoreApplication.translate("Form", u"Save shortcut to...", None))
#if QT_CONFIG(tooltip)
        self.forse_cbx.setToolTip(QCoreApplication.translate("Form", u"<html><head/><body><p>Force owerwrite existing file</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.forse_cbx.setText(QCoreApplication.translate("Form", u"F", None))
    # retranslateUi

