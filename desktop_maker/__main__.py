from PySide2.QtWidgets import QApplication
from desktop_maker.dialog import ShortcutMakerDialog

if __name__ == '__main__':
    app = QApplication([])
    w = ShortcutMakerDialog()
    w.show()
    app.exec_()